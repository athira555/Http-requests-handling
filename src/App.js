import React, { Component } from 'react';
import StudentProject from './containers/StudentProject/StudentProject';
import './App.css';
import { BrowserRouter } from 'react-router-dom';

class App extends Component {
  render() {

    return (
        <BrowserRouter>
      <div className="App">
      <StudentProject />
      </div>
    </BrowserRouter>
    );
  }

}

export default App;
