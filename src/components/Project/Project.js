import React from 'react';
import {ListGroup , ListGroupItem} from 'react-bootstrap';

const PersonRow = (props) => {
  return (
    <tr>
    <td>
      { props.key }
    </td>
      <td>
        { props.title }
      </td>
      <td>
        { props.desc }
      </td>
      <td>
        { props.inst }
      </td>
    </tr>
  );
}

export default PersonRow;
