import React from 'react';
import { Route } from 'react-router-dom';
import {Nav , NavItem} from 'react-bootstrap';
import NewProject from './NewProject/NewProject';
import ListProjects from './ListProjects/ListProjects';
class StudentProject extends React.Component {
  // handleSelect(selectedKey) {
  //   alert(`selected ${selectedKey}`);
  // }

  render() {
    return (
      <div>
      <Nav
       bsStyle="pills"
       justified
       //onSelect={key => this.handleSelect(key)}
     >
       <NavItem eventKey={1} href="/new-post">
         ADD NEW PROJECT
       </NavItem>
       <NavItem eventKey={2} title="Item" href="/">
         LIST ALL PROJECTS
       </NavItem>

     </Nav>

       <Route path="/new-post" exact component={NewProject}/>
        <Route path="/" exact component={ListProjects}/>
        </div>
      );
    }
  }
export default StudentProject;
