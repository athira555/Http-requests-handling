import React, { Component } from 'react';
import axios  from 'axios';
import {Form , FormGroup , FormControl , Col , ControlLabel , Button} from 'react-bootstrap';

class NewProject extends Component{

  state = {
    name: '',
    desc: '',
    inst:'',
    msg: '',
    show: false
  }
  postDataHandler = () => {

 const data = {
   project_name : this.state.name,
   project_description : this.state.desc,
   institution : this.state.inst
 }
 axios.post('http://192.168.16.23:8080/studentproject/add',data)
.then(response => {
    this.setState({msg : response.data});
});
alert("Project added!!!");

}


  render(){
    return(
      <div>
      <Form horizontal>
         <FormGroup controlId="formHorizontalName">
           <Col componentClass={ControlLabel} sm={2}>
                Project Name
              </Col>
           <Col sm={5}>
             <FormControl type="text" placeholder="Enter Project Name"
             value={this.state.name} onChange={(event) => this.setState({name: event.target.value})} />
           </Col>
         </FormGroup>

   <FormGroup controlId="formHorizontalDescription">
   <Col componentClass={ControlLabel} sm={2}>
     Project Description
   </Col>
    <Col sm={5}>
     <FormControl componentClass="textarea" placeholder="Enter Project Description"
     value={this.state.desc} onChange={(event) => this.setState({desc: event.target.value})}/>
     </Col>
   </FormGroup>

         <FormGroup controlId="formControlsInstitution">
         <Col componentClass={ControlLabel} sm={2}>
              Institution Name
            </Col>
           <Col sm={5}>
         <FormControl componentClass="select" placeholder="Select Institution"
         value={this.state.inst} onChange={(event) => this.setState({inst: event.target.value})}>
          <option value="NS">SELECT</option>
           <option value="GECI">GECI</option>
          <option value="TKM">TKM</option>
        </FormControl>
        </Col>
         </FormGroup>
        <FormGroup>
        <Col smOffset={2} sm={1}>
         <Button bsStyle="info" type="submit" onClick={this.postDataHandler}>Add Project</Button>
       </Col>
       </FormGroup>
     </Form>

     </div>
    );

  }
}

export default NewProject;
