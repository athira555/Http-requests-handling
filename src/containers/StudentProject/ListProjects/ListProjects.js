import React,{Component} from 'react';
import axios from 'axios';
import PersonRow from '../../../components/Project/Project';
import {Table} from 'react-bootstrap';

class ListProject extends Component {

  state = {
    projects : []
  }
  componentDidMount() {
  axios.get('http://192.168.16.23:8080/studentproject')
  .then(response => {
    const projects = response.data;
    const updatedProjects = projects.map(project => {
      return {
        ...project

      }
    });
    this.setState({projects : updatedProjects});

  });
}
render() {
  let rows = this.state.projects.map(person => {
      return <PersonRow key = {person.id}
      title = {person.project_name}
       desc={person.project_description}
        inst={person.institution}/>
    })

    return <Table striped bordered condensed hover>
            <thead>
            <tr>
             <th>Project Id</th>
             <th>Project Name</th>
             <th>Description</th>
             <th>Institution</th>
            </tr>
           </thead>
           <tbody>{rows}</tbody>
      < /Table>
  }
}
export default ListProject;
